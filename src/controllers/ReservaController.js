import Reserva from "../models/Reserva";

class ReservaController {
    async store(req, res) {
        const { user_id } = req.headers;
        const { id } = req.params;
        const { date } = req.body;

        const reserve = await Reserva.create({
            use: user_id,
            house: id,
            date
        })

        await reserve.populate('house').populate('user').execPopulate();

        return res.json(reserve)
    }
}

export default new ReservaController();