//metodos: index, show, update, store, destroy
/*
Index: listagem de sessões
store: criar uma sessão
show: Listar uma unica sessão
update: atualizar uma sessão
destroy: deletar uma sessão
*/

import User from '../models/User';

class SessionController {
    async store(req, res) {
        const { email } = req.body;

        //Verifico ja está cadastrado
        let user = await User.findOne({email});

        if(!user){
            user = await User.create({ email });
            return res.json(user);
        }else{
            return res.json(user);
        }        
    }
}

export default new SessionController();