import { Router } from 'express';
import SessionControler from './controllers/SessionController'
import HouseController from './controllers/HouseController';
import multer from 'multer';
import uploadConfig from './config/Upload'
import ReservaController from './controllers/ReservaController';

const routes = new Router();

const upload = multer(uploadConfig)

routes.post('/session', SessionControler.store);
routes.post('/houses', upload.single('thumbnail'), HouseController.store)
routes.get('/houses', HouseController.index);
routes.put('/houses/:id', upload.single('thumbnail'), HouseController.update);
routes.delete('/houses', HouseController.destroy);

routes.post('/houses/:id/reserva', ReservaController.store)

export default routes;